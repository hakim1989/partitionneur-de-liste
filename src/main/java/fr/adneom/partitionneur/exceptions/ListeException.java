package fr.adneom.partitionneur.exceptions;

/**
 * ListeException: exception spécifique à notre librairie. si besoin de faire un
 * traitement spécial en cas d'erreur.
 * 
 * @author hbousmal
 *
 */
public class ListeException extends Exception {

	/**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -8103191962286478937L;

	/**
	 * 
	 * Constructeur de la classe.
	 * 
	 * @param message
	 *            message.
	 */
	public ListeException(String message) {
		super(message);
	}

}
