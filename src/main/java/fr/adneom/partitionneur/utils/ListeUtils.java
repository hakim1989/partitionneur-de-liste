package fr.adneom.partitionneur.utils;

import java.util.ArrayList;
import java.util.List;

import fr.adneom.partitionneur.exceptions.ListeException;

public final class ListeUtils {
	/**
	 * Constructeur de la classe.
	 */
	private ListeUtils() {

	}

	/**
	 * fonction « partition » qui prend un paramètre « liste » et un paramètre «
	 * taille » et retourne une liste de sous liste, où chaque sous liste a au
	 * maximum « taille » éléments.
	 * 
	 * @param listeInteger
	 *            la liste des entiers a decouper
	 * @param taille
	 *            tailles des sous liste a retourner
	 * @return List<List<Integer>> la liste des sous liste.
	 */
	public static List<List<Integer>> partition(List<Integer> listeInteger, int taille) throws ListeException {
		if (listeInteger == null || listeInteger.isEmpty()) {
			throw new ListeException("La liste est vide est ne peut être partitionnée.");
		}
		if (taille <= 0) {
			throw new ListeException("la taille doit être supérieure à 0.");
		}
		List<List<Integer>> res = new ArrayList<>();
		while(listeInteger.size()>=taille) {
			res.add(listeInteger.subList(0, taille));
			listeInteger = listeInteger.subList(taille, listeInteger.size());
		}
		if(!listeInteger.isEmpty()) {
			res.add(listeInteger);
		}
		return res;
	}

}
