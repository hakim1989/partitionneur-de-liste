package fr.adneom.partitionneur.utils;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.junit.Test;

import fr.adneom.partitionneur.exceptions.ListeException;

public class ListeUtilsTest {

	/**
	 * testPartitionListeNull.
	 */
	@Test
	public void testPartitionListeNull() {
		List<Integer> listeInteger = null;
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 1);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNull(resultat);
	}

	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionListeVide() {
		List<Integer> listeInteger = new ArrayList<>();
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 1);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNull(resultat);
	}

	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionTaille1() {
		Integer[] valeurInitial = new Integer[] { 1, 2, 3, 4, 5 };
		List<Integer> listeInteger = Arrays.asList(valeurInitial);
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 1);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNotNull(resultat);
		assertEquals(resultat.size(), 5);
		assertEquals(resultat.get(0).get(0).intValue(), 1);
		assertEquals(resultat.get(1).get(0).intValue(), 2);
		assertEquals(resultat.get(2).get(0).intValue(), 3);
		assertEquals(resultat.get(3).get(0).intValue(), 4);
		assertEquals(resultat.get(4).get(0).intValue(), 5);
	}
	
	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionTaille2() {
		Integer[] valeurInitial = new Integer[] { 1, 2, 3, 4, 5 };
		List<Integer> listeInteger = Arrays.asList(valeurInitial);
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 2);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNotNull(resultat);
		assertEquals(resultat.size(), 3);
		assertEquals(resultat.get(0).get(0).intValue(), 1);
		assertEquals(resultat.get(0).get(1).intValue(), 2);
		assertEquals(resultat.get(1).get(0).intValue(), 3);
		assertEquals(resultat.get(1).get(1).intValue(), 4);
		assertEquals(resultat.get(2).get(0).intValue(), 5);
	}
	
	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionTaille3() {
		Integer[] valeurInitial = new Integer[] { 1, 2, 3, 4, 5 };
		List<Integer> listeInteger = Arrays.asList(valeurInitial);
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 3);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNotNull(resultat);
		assertEquals(resultat.size(), 2);
		assertEquals(resultat.get(0).get(0).intValue(), 1);
		assertEquals(resultat.get(0).get(1).intValue(), 2);
		assertEquals(resultat.get(0).get(2).intValue(), 3);
		assertEquals(resultat.get(1).get(0).intValue(), 4);
		assertEquals(resultat.get(1).get(1).intValue(), 5);
	}
	
	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionTaille4() {
		Integer[] valeurInitial = new Integer[] { 1, 2, 3, 4, 5 };
		List<Integer> listeInteger = Arrays.asList(valeurInitial);
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 4);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNotNull(resultat);
		assertEquals(resultat.size(), 2);
		assertEquals(resultat.get(0).get(0).intValue(), 1);
		assertEquals(resultat.get(0).get(1).intValue(), 2);
		assertEquals(resultat.get(0).get(2).intValue(), 3);
		assertEquals(resultat.get(0).get(3).intValue(), 4);
		assertEquals(resultat.get(1).get(0).intValue(), 5);
	}
	
	/**
	 * testPartitionListeVide.
	 */
	@Test
	public void testPartitionTaille5() {
		Integer[] valeurInitial = new Integer[] { 1, 2, 3, 4, 5 };
		List<Integer> listeInteger = Arrays.asList(valeurInitial);
		List<List<Integer>> resultat = null;
		try {
			resultat = ListeUtils.partition(listeInteger, 5);
		} catch (ListeException e) {
			e.printStackTrace();
		}
		assertNotNull(resultat);
		assertEquals(resultat.size(), 1);
		assertEquals(resultat.get(0).get(0).intValue(), 1);
		assertEquals(resultat.get(0).get(1).intValue(), 2);
		assertEquals(resultat.get(0).get(2).intValue(), 3);
		assertEquals(resultat.get(0).get(3).intValue(), 4);
		assertEquals(resultat.get(0).get(4).intValue(), 5);
	}

}
